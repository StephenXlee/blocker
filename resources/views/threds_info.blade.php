@extends('layouts.app')
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>


@section('content')

<div class="container">
    <!-- Begin of rows -->
    <div class="row carousel-row">
        <div class="col-xs-8 col-xs-offset-2 slide-row">
            <div class="slide-content">
                <h4>{{$newthreds->title}}</h4>
                <p>{{$newthreds->message}}</p>
            </div>
            <div class="slide-footer">
               <ul class="list-inline list-unstyled">
                <li><span><i class="glyphicon glyphicon-calendar"></i>{{$newthreds->created_at->diffForHumans()}}</span></li>
                <li>|</li>
                <span><i class="glyphicon glyphicon-comment">{{count($newthreds->likes)}}</i></span>
                 @guest
                 @else
                <form action="{{route('thredLike', $newthreds->id)}}" method="post">
                    {{ csrf_field() }}
                    <span class="pull-right buttons">
                        <button type="submit" class="btn btn-sm btn-primary"> Like</button>
                    </span>
                </form>
                @endguest
            </ul>
            </div>
        </div>
    </div>
    <div class="row carousel-row">
    </div>
</div>

<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css"/>
<div class="container">
  <div class="well">
    @foreach ($newthreds->comments as $comment)  
      <div class="media"> 
        <div class="media-body">
            <h4 class="media-heading">{{$comment->user->name}}</h4>
          <p class="text-right">{{$comment->user->email}}</p>
          <p>{{$comment->message}}</p>
            <ul class="list-inline list-unstyled">
                <li><span><i class="glyphicon glyphicon-calendar"></i> {{$comment->created_at->diffForHumans()}} </span></li>
                <li>|</li>
                <span><i class="glyphicon glyphicon-comment"></i>{{count($comment->likes) }}</span>
                 @guest
                 @else
                <form action="{{route('commentLike', $comment->id)}}" method="post">
                    {{ csrf_field() }}
                    <span class="pull-right buttons">
                        <button type="submit" class="btn btn-sm btn-primary"> Like</button>
                    </span>
                </form>
                @endguest
            </ul>
       </div>
    </div>
@endforeach
  </div>
</div>

                        @guest
                            <li><a href="{{ route('login') }}">you need to log in first to leave a comment</a></li>
                        @else
                           <div class="container">
    <div class="row">
        <h3>comment</h3>
    </div>
    
    <div class="row">
    
    <div class="col-md-6">
                            <div class="widget-area no-padding blank">
                                <div class="status-upload">
                                    <form action="{{route('comment_upload',$newthreds->id)}}" method="post">
                                         {{ csrf_field() }}
                                        <input  name="comment" type="text" class="span3" placeholder="your comment">
                                        <button type="submit" class="btn btn-success green"><i class="fa fa-share"></i> Share</button>
                                    </form>
                                </div><!-- Status Upload  -->
                            </div><!-- Widget Area -->
                        </div>
        
    </div>
</div>
                        @endguest
@endsection

