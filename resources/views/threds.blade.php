@extends('layouts.app')

@section('content')
 
<div class="container">
    <div class="row carousel-row">
        <div class="col-xs-8 col-xs-offset-2 slide-row">  
        @foreach($newthreds as $newthred)             
            <div class="slide-content">
                <h2>{{$newthred->author->name }}<br>{{ $newthred->title}}</h2>
                <h4>{{$newthred->author->email}}</h4>
                <p>
                    {{$newthred->message}}
                </p>
            </div>
           
            <div class="slide-footer">
                <span class="pull-right buttons">
                    <button class="btn btn-sm btn-default"><a href="{{ route('threds_info', $newthred->id)}}">Show</a></button>
                    @if(Auth::user()->threds->contains('id',$newthred->id) )
                    <button class="btn btn-sm btn-default"> <a href="{{route('edit_thred', $newthred->id)}}">Edit</a></button>
                     <button class="btn btn-sm btn-default"> <a href="{{route('delete_thred', $newthred->id)}}">Delete</a></button>
                    @endif
                </span>
            </div>
            @endforeach
        </div>
    </div>
</div>


@endsection