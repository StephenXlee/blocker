
<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="span6">
            <form action="{{route('addthred')}}" method="post">
                <div class="controls controls-row">
                    {{ csrf_field() }}
                    <input id="name" name="title" type="text" class="span3" placeholder="title">
                </div>
                <div class="controls">
                    <textarea id="message" name="message" class="span6" placeholder="Your Message" rows="5"></textarea>
                </div>
                  
                <div class="controls">
                    <button id="contact-submit" type="submit" class="btn btn-primary input-medium pull-right">Send</button>
                </div>
            </form>
        </div>
	</div>
</div>
@endsection