<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Thred;
use Auth;

class ThredsController extends Controller
{
   public function NewThred(Request $request){
	    $user_id = Auth::user()->id;
	    $title = $request->input('title');
	    $message = $request->input('message');
	    $newthred= new Thred;
	    $newthred->user_id=$user_id;
	    $newthred->title=$title;
	    $newthred->message=$message;
	    $newthred->save();
	    return redirect()->route('threds');
	}

	public function GetThred(){
    	$newthreds = Thred::all();
    	return view('threds', compact('newthreds'));

    }

    public function GetThredInfo($id){
    	$newthreds = Thred::find($id);
    	return view('threds_info', compact('newthreds'));
    }

    public function GetMyThred(){
    	$newthreds =Auth::user()->threds;
       return view('threds', compact('newthreds'));
    }

    public function DeleteThred($id){
    	Thred::destroy($id);
    	return back();
    }

    public function EditThred($id){
    $editthred = Thred::find($id);
    return view('edit_thred', compact('editthred'));
    }

    public function EditThredDB(Request $request,$id){
        $edittodata = Thred::find($id);
        $user_id= Auth::user()->id;
        $title = $request->input('title');
        $message = $request->input('message');
        $edittodata->user_id=$user_id;
        $edittodata->title=$title;
        $edittodata->message=$message;
        $edittodata->save();

        $newthreds =Auth::user()->threds;
         return view('threds', compact('newthreds'));
    }
}
