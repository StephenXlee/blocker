<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class LikeController extends Controller
{
     public function CommentLike(Request $request ,$id){
	   	// $comment = CommentSection::find($id);

	   	if(Auth::user()->commentLikes->contains($id)){
	   		return back();
	   	}
	   	Auth::user()->commentLikes()->attach($id);
	    return back();
	}    

	public function ThredLike(Request $request, $id){
         
         if( Auth::user()->thredLikes->contains($id)){
			return back();
		}
 		Auth::user()->thredLikes()->attach($id);
	    return back();
	}
}
