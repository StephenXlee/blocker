<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CommentSection;
use Auth;


class CommentsContoller extends Controller
{
     public function newComment(Request $request ,$id){
	    $message = $request->input('comment');
	    $post_id = $id;
	    $user_id = Auth::user()->id;
	    $newcomment= new CommentSection;
	    $newcomment->post_id=$post_id;
	    $newcomment->user_id=$user_id;
	    $newcomment->message=$message;
	    $newcomment->save();
	    // return redirect()->route('threds_info');
	     return back();
	}    
}

