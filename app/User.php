<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function commentLikes()
    {
        return $this-> belongsToMany('App\CommentSection','comment_likes','user_id','comment_id');
    }

    public function thredLikes()
    {
        return $this->belongsToMany('App\Thred','thred_likes','user_id','thred_id');
    }

    public function threds()
    {
         return $this->hasMany('App\Thred');
    }
}
