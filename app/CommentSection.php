<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentSection extends Model
{
     public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function likes()
	{
		return $this->belongsToMany('App\User','comment_likes','comment_id','user_id');
	}
}
