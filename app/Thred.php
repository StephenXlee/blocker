<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Thred extends Model
{
    public function comments()
 	{
    	return $this->hasMany('App\CommentSection','post_id');
	}
	public function likes()
	{
		return $this->belongsToMany('App\User','thred_likes','thred_id','user_id');
	}
	public function author()
	{
		return $this->belongsTo('App\User','user_id');
	}
}
