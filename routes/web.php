
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/newthred', function() {
    return view('new_thred');
})->name('new_thred');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/threds/thredsinfo/thredslike/{id}', 'LikeController@ThredLike')->name('thredLike');
Route::post('/threds/thredsinfo/thredsComment/commentLike/{id}','LikeController@CommentLike')->name('commentLike');
Route::post('/addthred', 'ThredsController@NewThred')->name('addthred');
Route::get('/threds', 'ThredsController@GetThred')->name('threds');
Route::get('/threds/thredsinfo/{id}', 'ThredsController@GetThredInfo')->name('threds_info');
Route::post('/threds/thredsinfo/thredsComment/{id}', 'CommentsContoller@newComment')->name('comment_upload');
Route::get('/threds/mythreds','ThredsController@GetMyThred')->name('user_thred');
Route::get('/threds/myThreds/deleteThred/{id}','ThredsController@DeleteThred')->name('delete_thred');
Route::get('threds/myThreds/editThred/{id}','ThredsController@EditThred')->name('edit_thred');
Route::post('threds/myThreds/editThred/data/{id}','ThredsController@EditThredDB')->name('edit_thred_db');